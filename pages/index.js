// import Image from "next/image";
"use client"; // This is a client component 👈🏽
//import { useState } from "react";
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import React from 'react';
//import { findFiles } from '@/app/findfiles.js';

const spanStyle = {
  padding: '20px',
  background: '#efefef',
  color: '#F00000'
}

const divStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundSize: 'cover',
  height: '1080px',
  width: '1920px'
}



export async function getServerSideProps() {
  const fs = require('node:fs');
  const path = require('node:path');
  var slideImages = [];
  var result = [];
    const ext='.jpg';
    const dir = path.join('public' , 'images');
    // const dir='public/images/';
    const imageFiles = fs.readdirSync(dir).filter(file => file.endsWith(ext));
    for (const file of imageFiles) {
      const filePath = path.join('images', file);
      slideImages.push({url: filePath});
    }

    // fs.readdir(dir, (err, files) => {
    //     files.forEach(file => {
    //     if(file.endsWith(ext)) {
    //         result.push({ url: `images/${file}`});
    //     }
    //     console.log(file);
    //     });
    // });

  // const slideImages = [
  //   {
  //     url: 'images/xp.jpg'
  //   },
  //   {
  //     url: 'https://images.unsplash.com/photo-1506710507565-203b9f24669b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1536&q=80'
  //   },
  //   {
  //     url: 'https://images.unsplash.com/photo-1536987333706-fc9adfb10d91?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80'
  //   },
  // ];

  return {props: {slideImages} }
}


export default function Home( {slideImages} ) {
  console.log(slideImages);
  return (
    <div className="slide-container">
        <Slide>
         {slideImages.map((slideImage, index)=> (
            <div key={index}>
              <div style={{ ...divStyle, 'backgroundImage': `url(${slideImage.url})` }}>
              </div>
            </div>
          ))} 
        </Slide>
      </div>
  )
}
